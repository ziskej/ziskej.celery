from ziskej.celery.api_server import app
from ziskej.celery.config import ZISKEJ_ASYNC_ROOT


app.config.from_pyfile(ZISKEJ_ASYNC_ROOT + "config/gunicorn/config.py")

if __name__ == "__main__":
    app.run()
