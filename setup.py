# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open('README.rst', encoding='utf-8') as f:
        return f.read()


setup(
    name='ziskej.celery',
    version='2.9.1',
    description='Flask wrapper and Celery tasks for Ziskej',
    long_description=readme(),
    # https://pypi.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'License :: Other/Proprietary License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules',
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
    ],
    keywords='ziskej',
    url='',
    author='AOW',
    author_email='aow@aow.cz',
    license='GPL version 2',
    packages=['ziskej.celery'],
    install_requires=[
        'celery[redis]~=4.1.0',
        'requests',
        'flask',
        'urllib3',
        'xmltodict',
        'pyjwt',
        'cryptography',
    ],
    entry_points={
    },
    include_package_data=True,
    zip_safe=False)
