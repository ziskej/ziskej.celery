# -*- coding: utf-8 -*-

"""Získej API pro CPK pro čtenáře.  Pomocná obecná funkcionalita, např.
zabezpečení pomocí JWT.
"""

import datetime
import jwt

from ziskej.celery.config import (
    ZISKEJ_API_ID,
    SHARED_SECRET_ZISKEJ_APP,
    )


DEBUG = True


class ApiException(Exception):
    pass


class ApiException401(ApiException):
    pass


class ApiException403(ApiException):
    pass


class ApiException422(ApiException):
    pass


class ApiException500(ApiException):
    pass


class ZiskejBase(object):
    """Získej API pro CPK pro čtenáře.  Pomocná obecná funkcionalita, např.
    zabezpečení pomocí JWT.
    """

    def __init__(self):
        self.ziskej_api_id = ZISKEJ_API_ID
        # for _parse_jwt_ziskej_app we need flask request
        self.request = None  # flask request

    def _create_jwt_ziskej_app(self):
        """Vytvořit JWT pro request posílaný do Získej app."""
        now = datetime.datetime.utcnow()
        token_dict = dict(
            iss = self.ziskej_api_id,
            iat = now,
            exp = now + datetime.timedelta(seconds=3600),
            app = self.ziskej_api_id,
            )
        token = jwt.encode(token_dict, SHARED_SECRET_ZISKEJ_APP, algorithm='HS256')
        #print(token)
        try:
            result = token.decode('utf-8')
        except AttributeError:
            result = token
        return result

    def _parse_jwt_ziskej_app(self):
        """Ověřit JWT pro request ze Získej app."""

        if DEBUG:
            print("_parse_jwt_ziskej_app")

        DEBUG_SKIP_JWT = False
        if DEBUG_SKIP_JWT:
            if DEBUG:
                print("DEBUG_SKIP_JWT")
            return None

        if self.request is None:
            raise ApiException500('Missing configuration')

        header_authorization = self.request.headers.get('Authorization')
        if DEBUG:
            print("header_authorization:", header_authorization)
        if not header_authorization:
            raise ApiException401('Missing authorization')
        if not header_authorization.startswith('bearer '):
            raise ApiException401('Invalid authorization format')
        token = header_authorization[7:]

        try:
            token_dict = jwt.decode(token, SHARED_SECRET_ZISKEJ_APP, algorithms=['HS256'])
        except jwt.ExpiredSignatureError as e:
            logger.warning('ExpiredSignatureError: {msg}'.format(msg=str(e)))
            raise ApiException401('Expired authorization token')
        except jwt.exceptions.InvalidTokenError as e:
            logger.error('InvalidTokenError: {msg}'.format(msg=str(e)))
            raise ApiException401('Invalid authorization token')

        if not token_dict.get('iat', ''):
            raise ApiException401('Missing iat in authorization token')
        if not token_dict.get('exp', ''):
            raise ApiException401('Missing exp in authorization token')
        if token_dict.get('iss', '') != ZISKEJ_API_ID:
            print("iss:", token_dict.get('iss', ''))
            print("ZISKEJ_API_ID:", ZISKEJ_API_ID)
            raise ApiException401('Invalid iss')
        if token_dict.get('app', '') != ZISKEJ_API_ID:
            raise ApiException401('Invalid app')

        return token_dict
