# -*- coding: utf-8 -*-

"""Konfigurace, specifická pro jednotlivé instance.
"""

import os


class Config(object):
    JSON_AS_ASCII = False

    DEBUG = False
    TESTING = False


class ProdConfig(Config):
    pass


class DemoConfig(Config):
    pass


class TestConfig(Config):
    pass


class DevConfig(Config):
    DEBUG = True
    TESTING = True


class LocalConfig(Config):
    DEBUG = True
    TESTING = True


# shared secret used for JWT between Ziskej API Flask and Ziskej app Zope
SHARED_SECRET_ZISKEJ_APP = 'F05}ID5t]_YT4z+8,liNbnO/3Q9</7Xo'

# shared secret used for JWT between CPK and Ziskej API
SHARED_SECRET_ZISKEJ_API = 'O:pc"6s5D>Z!YvN,r:VE7iLr(9Vx73o$'

# root directory for Ziskej API Flask installation
ZISKEJ_ASYNC_ROOT = os.environ.get("ZISKEJ_ASYNC_ROOT", default=None)
if ZISKEJ_ASYNC_ROOT is None:
    print("ZISKEJ_ASYNC_ROOT is missing in ENV, using default /data/ziskej-async/")
    ZISKEJ_ASYNC_ROOT = '/data/ziskej-async/'
ZISKEJ_ASYNC_ROOT = str(ZISKEJ_ASYNC_ROOT)
if not os.path.isdir(ZISKEJ_ASYNC_ROOT):
    raise Exception('Invalid ZISKEJ_ASYNC_ROOT in ENV, directory does not exist.')
if not ZISKEJ_ASYNC_ROOT.endswith('/'):
    ZISKEJ_ASYNC_ROOT += '/'

# Configuration
# see http://flask.pocoo.org/docs/1.0/config/#development-production
ZISKEJ_ASYNC_ENABLED = True
ZISKEJ_API_ENABLED = False
JWT_ENABLED = True
ZISKEJ_API_BASE_URL = 'https://ziskej.techlib.cz/'  # použito v dokumentaci
ZISKEJ_APP_BASE_URL = None  # použito v Získej API
CERT_DIR = ZISKEJ_ASYNC_ROOT + 'certs/'
HEADER_INSTANCES = ('test', 'prod', )

ZISKEJ_ASYNC_MODE = os.environ.get("ZISKEJ_ASYNC_MODE", default=None)
if ZISKEJ_ASYNC_MODE == 'PROD':
    ZISKEJ_API_ENABLED = True
    ZISKEJ_API_BASE_URL = 'https://ziskej.techlib.cz/'
    ZISKEJ_APP_BASE_URL = 'https://ziskej.techlib.cz/'
elif ZISKEJ_ASYNC_MODE == 'DEMO':
    pass
elif ZISKEJ_ASYNC_MODE == 'TEST':
    ZISKEJ_API_ENABLED = True
    ZISKEJ_API_BASE_URL = 'https://ziskej-test.techlib.cz/'
    ZISKEJ_APP_BASE_URL = 'https://ziskej-test.techlib.cz/'
elif ZISKEJ_ASYNC_MODE == 'DEV':
    pass
elif ZISKEJ_ASYNC_MODE == 'LOCAL':
    ZISKEJ_API_ENABLED = True
    ZISKEJ_API_BASE_URL = 'http://localhost:8000/'
    ZISKEJ_APP_BASE_URL = 'http://ziskej.private/'
else:
    raise Exception('Missing ZISKEJ_ASYNC_MODE in ENV.')
ZISKEJ_API_ID = 'ziskej-async.{mode}'.format(mode=ZISKEJ_ASYNC_MODE)
CPK_APP_ID = 'cpk'

print("ZISKEJ_ASYNC_ROOT:", ZISKEJ_ASYNC_ROOT)
print("ZISKEJ_ASYNC_MODE:", ZISKEJ_ASYNC_MODE)
print("ZISKEJ_API_ID:", ZISKEJ_API_ID)
print("ZISKEJ_ASYNC_ENABLED:", ZISKEJ_ASYNC_ENABLED)
print("ZISKEJ_API_ENABLED:", ZISKEJ_API_ENABLED)
print("JWT_ENABLED:", JWT_ENABLED)
print("ZISKEJ_API_BASE_URL:", ZISKEJ_API_BASE_URL)
print("ZISKEJ_APP_BASE_URL:", ZISKEJ_APP_BASE_URL)
