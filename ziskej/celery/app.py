from celery import Celery

REDIS_URL = 'redis://localhost:6379/0'

app = Celery('tasks',
             broker=REDIS_URL,
             backend=REDIS_URL,
             include=['ziskej.celery.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
)

if __name__ == '__main__':
    app.start()

