# -*- coding: utf-8 -*-

"""Flask wrapper pro CPK API, různá NTK API a Získej API pro CPK pro čtenáře.
Celery workery a tasky pro asynchronní volání.  Samotný Fkask wrapper obsahuje
synchronní (blokující) i asynchronní (neblokující) části.

Používané prefixy pro konstrukci URI:

    /cpkapi/
    Prefix pro wrapper pro CPK API.

    /ntkapi/
    Prefix pro wrapper pro různá API v rámci NTK.

    /api/
    Prefix pro Získej API pro CPK pro čtenáře.
"""

import datetime
import json
import jwt
import logging
import os
import requests

from flask import (
    Flask,
    request,
    jsonify,
    render_template,
    )
import celery

from ziskej.celery.config import *

from ziskej.celery.tasks import (
    cpk_libraries,
    cpk_library,
    cpk_documents_overview,
    cpk_document,
    cpk_unit,
    app as celery_app,
)

from ziskej.celery.ziskej_api import ZiskejApi

from ziskej.celery.ntk_payment import (
    NtkPaymentException,
    NtkPayment,
    )

from ziskej.celery.ntk_platebator import (
    NtkPlatebatorException,
    NtkPlatebator,
    )


app = Flask(__name__, template_folder='templates')

@app.before_first_request
def setup_logging():
    app.logger.addHandler(logging.StreamHandler())
    app.logger.setLevel(logging.INFO)

logger = app.logger

if ZISKEJ_ASYNC_MODE == 'PROD':
    app.config.from_object('ziskej.celery.api_server.ProdConfig')
elif ZISKEJ_ASYNC_MODE == 'DEMO':
    app.config.from_object('ziskej.celery.api_server.DemoConfig')
elif ZISKEJ_ASYNC_MODE == 'TEST':
    app.config.from_object('ziskej.celery.api_server.TestConfig')
elif ZISKEJ_ASYNC_MODE == 'DEV':
    app.config.from_object('ziskej.celery.api_server.DevConfig')
elif ZISKEJ_ASYNC_MODE == 'LOCAL':
    app.config.from_object('ziskej.celery.api_server.LocalConfig')
else:
    raise Exception('Missing ZISKEJ_ASYNC_MODE in ENV.')


# Root
@app.route("/")
def root():
    return jsonify(result='Invalid request')


# CPK API wrapper

#@app.route("/cpk/<method>")
@app.route("/cpkapi/async/init/<method>")
def cpkapi_async_init(method):
    """Asynchronní (neblokující) verze wrapperu nad CPK API, inicializace
    metody.
    """

    max_retries = int(request.args.get('max_retries', 1))

    if method == 'libraries':
        limit_on_page = int(request.args.get('limit_on_page', 0))
        starting_page = int(request.args.get('starting_page', 1))
        page_limit = int(request.args.get('page_limit', 0))
        r = cpk_libraries.delay(limit_on_page=limit_on_page,
                                starting_page=starting_page,
                                page_limit=page_limit,
                                max_retries=max_retries)

    elif method == 'library':
        r = cpk_library.delay(library_id=request.args['id'],
                              max_retries=max_retries)

    elif method == 'document':
        r = cpk_document.delay(document_id=request.args['id'],
                               max_retries=max_retries)

    elif method == 'unit':
        r = cpk_unit.delay(unit_id=request.args['id'],
                           max_retries=max_retries)

    return jsonify(dict(data = dict(rid = r.id), status = 'OK'))

#@app.route("/retrieve/<result_id>")
@app.route("/cpkapi/async/retrieve/<result_id>")
def cpkapi_async_retrieve(result_id):
    """Asynchronní (neblokující) verze wrapperu nad CPK API, výsledek.
    """

    r = celery_app.AsyncResult(result_id)
    if r.status == 'FAILURE':
        return jsonify(dict(error_message = 'FAILURE', status = 'Error'))

    try:
        return jsonify(r.get(timeout=1))
    except celery.exceptions.TimeoutError:
        return jsonify(dict(status = 'NotYet'))

@app.route("/cpkapi/monitor")
def cpkapi_monitor():
    """Monitor CPK API, odchytí například chybnou konfiguraci CPK, která
    neakceptuje volání ze Získej (TEST / DEMO / PROD) virtuálu.
    """

    r = cpk_library(library_id='library.000000732',  # MZK
                    max_retries=1,
                    logger=logger)
    if r.get('status', None) == 'Error':
        return jsonify(r), 500
    return jsonify(dict(status='OK'))

#@app.route("/cpkd/<method>")
@app.route("/cpkapi/sync/<method>")
def cpkapi_sync(method):
    """Synchronní (blokující) verze wrapperu nad CPK API, metoda.
    """

    max_retries = int(request.args.get('max_retries', 1))

    if method == 'libraries':
        limit_on_page = int(request.args.get('limit_on_page', 0))
        starting_page = int(request.args.get('starting_page', 1))
        page_limit = int(request.args.get('page_limit', 0))
        r = cpk_libraries(limit_on_page=limit_on_page,
                          starting_page=starting_page,
                          page_limit=page_limit,
                          max_retries=max_retries,
                          logger=logger)

    elif method == 'library':
        r = cpk_library(library_id=request.args['id'],
                        max_retries=max_retries,
                        logger=logger)

    elif method == 'documents_overview':
        ids_str = request.args.get('ids', '')
        ids = ids_str.split(',')
        r = cpk_documents_overview(
                document_ids=ids,
                max_retries=max_retries,
                logger=logger)

    elif method == 'document':
        r = cpk_document(document_id=request.args['id'],
                         max_retries=max_retries,
                         logger=logger)

    elif method == 'unit':
        r = cpk_unit(unit_id=request.args['id'],
                     max_retries=max_retries,
                     logger=logger)

    return jsonify(r)


# Obecné utility pro NTK API a Získej API

def get_flask_post_data():
    data = None
    use_post = bool(request.method not in ('GET', ))
    if use_post and request.data:
        value = request.data.decode('utf-8')
        try:
            data = json.loads(value)
        except Exception as e:
            logger.exception(str(e))
            raise Exception('Invalid json.')
    return data


# Platební systém NTK

#@app.route('/ntk/users/<ntk_id>/credit')
@app.route('/ntkapi/users/<ntk_id>/credit')
def ntkapi_users_credit(ntk_id):
    """Methods:

    /ntkapi/users/:ntk_id/credit GET
    Zjistit stav kreditu v NTK uživatele. NTK
    """

    header_instance = request.headers.get('X-Ziskej-Instance', '')
    if header_instance not in HEADER_INSTANCES:
        logger.info('ntk_credit: Invalid instance info')
        return jsonify(dict(error = 'Invalid instance info')), 422
    ntkpayment = NtkPayment(request, jsonify, header_instance, logger)
    return ntkpayment.credit(ntk_id)

#@app.route('/ntk/users/<ntk_id>/payments', methods=['POST'])
@app.route('/ntkapi/users/<ntk_id>/payments', methods=['POST'])
def ntkapi_users_payment(ntk_id):
    """Methods:

    /ntkapi/users/:ntk_id/payments POST
    Zaplatit z kreditu v NTK uživatele NTK.
    """

    header_instance = request.headers.get('X-Ziskej-Instance', '')
    if header_instance not in HEADER_INSTANCES:
        logger.info('ntk_payment: Invalid instance info')
        return jsonify(dict(error = 'Invalid instance info')), 422
    data = get_flask_post_data()
    ntkpayment = NtkPayment(request, jsonify, header_instance, logger)
    return ntkpayment.pay(ntk_id, data)


# Platebátor API

#@app.route('/ntk/platebator', methods=['POST'])
@app.route('/ntkapi/platebator', methods=['POST'])
def ntkapi_platebator_create():
    """Methods:

    /ntkapi/platebator POST {payment*}
    Vytvořit platbu v platebátoru.
    """

    header_instance = request.headers.get('X-Ziskej-Instance', '')
    if header_instance not in HEADER_INSTANCES:
        return jsonify(dict(error = 'Invalid instance info')), 422
    data = get_flask_post_data()
    ntkplatebator = NtkPlatebator(request, jsonify, header_instance, logger)
    return ntkplatebator.create_data(data)

#@app.route('/ntk/platebator/<platebator_id>')
@app.route('/ntkapi/platebator/<platebator_id>')
def ntkapi_platebator_check(platebator_id):
    """Methods:

    /ntkapi/platebator/:platebator_id GET
    Zjistit stav platby.
    """

    header_instance = request.headers.get('X-Ziskej-Instance', '')
    if header_instance not in HEADER_INSTANCES:
        return jsonify(dict(error = 'Invalid instance info')), 422
    service = request.args.get('service', 'mvs')
    if service not in ('mvs', 'edd', ):
        service = 'mvs'
    ntkplatebator = NtkPlatebator(request, jsonify, header_instance, logger)
    return ntkplatebator.check(platebator_id, service=service)


# Získej API pro CPK pro čtenáře

@app.route('/api/v1/')
def api_docs():
    """Methods:

    /api/v1/ GET
    Vrátit api dokumentaci.
    """

    api_base_url = '{}api/v1'.format(ZISKEJ_API_BASE_URL)
    return render_template('ziskej_api_docs.html', api_base_url=api_base_url)

@app.route('/api/v1/login', methods=['POST'])
def api_login():
    """Methods:

    /api/v1/login POST
    Vrátit token s expirací za 1 hodinu, pokud jsou přihlašovací údaje korektní,
    povoleno jen na TESTu.
    """

    ziskej_api = ZiskejApi(request, jsonify, app.config['DEBUG'], logger)
    return ziskej_api.login()

@app.route('/api/v1/libraries')
def api_libraries():
    """Methods:

    /api/v1/libraries GET
    Vrátit seznam knihoven v Ziskej.
    """

    ziskej_api = ZiskejApi(request, jsonify, app.config['DEBUG'], logger)
    service = request.args.get('service', None)
    include_deactivated = request.args.get('include_deactivated', None)
    return ziskej_api('api_cpk_libraries_get', public=True, service=service,
                      include_deactivated=include_deactivated)

@app.route('/api/v1/service/edd/estimate')
def api_edd_estimate():
    """Methods:

    /api/v1/service/edd/estimate POST
    Odhadnout cenu EDD pro čtenáře a zda je takový rozsah umožněn (Dília).
    """

    ziskej_api = ZiskejApi(request, jsonify, app.config['DEBUG'], logger)
    if request.method == 'GET':
        number_of_pages = request.args.get('number_of_pages', None)
        edd_subtype = request.args.get('edd_subtype', None)
    return ziskej_api('api_cpk_service_edd_estimate', public=True, number_of_pages=number_of_pages, edd_subtype=edd_subtype)

@app.route('/api/v1/readers/<eppn>', methods=['GET', 'PUT', 'DELETE'])
def api_readers_id(eppn):
    """Methods:

    /api/v1/readers/:eppn GET
    Vrátit profil čtenáře na základě jeho eppn.

    /api/v1/readers/:eppn PUT
    Vytvořit nebo aktualizovat čtenáře v Získej.

    /api/v1/readers/:eppn DELETE
    Smazat čtenáře v Získej včetně všech jeho objednávek, povoleno jen na TESTu.
    """

    ziskej_api = ZiskejApi(request, jsonify, app.config['DEBUG'], logger)
    if request.method == 'GET':
        expand = request.args.get('expand', None)
        return ziskej_api('api_cpk_readers_eppn_get', eppn=eppn, expand=expand)
    if request.method == 'PUT':
        return ziskej_api('api_cpk_readers_eppn_put', eppn=eppn)
    if request.method == 'DELETE':
        return ziskej_api('api_cpk_readers_eppn_delete', eppn=eppn)

@app.route('/api/v1/readers/<eppn>/tickets', methods=['GET', 'POST'])
def api_tickets(eppn):
    """Methods:

    /api/v1/readers/:eppn/tickets GET
    Vrátit všechny otevřené objednávky čtenáře.

    /api/v1/readers/:eppn/tickets POST
    Vytvořit novou objednávku čtenáře.
    """

    ziskej_api = ZiskejApi(request, jsonify, app.config['DEBUG'], logger)
    if request.method == 'GET':
        expand = request.args.get('expand', None)
        include_closed = request.args.get('include_closed', None)
        ticket_type = request.args.get('ticket_type', None)
        ticket_doc_data_source = request.args.get('ticket_doc_data_source', None)
        edd_subtype = request.args.get('edd_subtype', None)
        return ziskej_api('api_cpk_tickets_get',
            eppn=eppn, expand=expand,
            include_closed=include_closed, ticket_type=ticket_type,
            ticket_doc_data_source=ticket_doc_data_source,
            edd_subtype=edd_subtype)
    if request.method == 'POST':
        return ziskej_api('api_cpk_tickets_post', eppn=eppn)

@app.route('/api/v1/readers/<eppn>/tickets/<ticket_id>', methods=['GET', 'DELETE'])
def api_tickets_id(eppn, ticket_id):
    """Methods:

    /api/v1/readers/:eppn/tickets/:ticket_id GET
    Vrátit detail objednávky čtenáře.
    """

    ziskej_api = ZiskejApi(request, jsonify, app.config['DEBUG'], logger)
    if request.method == 'GET':
        return ziskej_api('api_cpk_tickets_id_get', eppn=eppn, ticket_id=ticket_id)
    if request.method == 'DELETE':
        return ziskej_api('api_cpk_tickets_id_delete', eppn=eppn, ticket_id=ticket_id)

@app.route('/api/v1/readers/<eppn>/tickets/<ticket_id>/messages', methods=['GET', 'POST', 'PUT'])
def api_messages(eppn, ticket_id):
    """Methods:

    /api/v1/readers/:eppn/tickets/:ticket_id/messages GET
    Vrátit seznam všech zpráv pro objednávku.

    /api/v1/readers/:eppn/tickets/:ticket_id/messages POST
    Vytvořit novou zprávu pro objednávku.

    /api/v1/readers/:eppn/tickets/:ticket_id/messages PUT
    Označit všechny zprávy pro objednávku jako přečtené.
    """

    ziskej_api = ZiskejApi(request, jsonify, app.config['DEBUG'], logger)
    if request.method == 'GET':
        return ziskej_api('api_cpk_messages_get', eppn=eppn, ticket_id=ticket_id)
    if request.method == 'POST':
        return ziskej_api('api_cpk_messages_post', eppn=eppn, ticket_id=ticket_id)
    if request.method == 'PUT':
        return ziskej_api('api_cpk_messages_put', eppn=eppn, ticket_id=ticket_id)
