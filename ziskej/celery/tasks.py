# -*- coding: utf-8 -*-
"""CPK API Celery tasks, pro asynchronní volání.
"""

import requests
from time import sleep
from xmltodict import parse, unparse

from ziskej.cpk_api import (
    CPK,
    CPKException,
    Document,
    cpk_docs_overview,
    cpk_doc,
    )

from .app import app


@app.task(bind=True, default_retry_delay=10)
def cpk_libraries(self, limit_on_page=50, starting_page=1, page_limit=0,
                  max_retries=0, logger=None):
    cpk = CPK(logger)
    return cpk.libraries(limit_on_page=limit_on_page,
                         starting_page=starting_page,
                         page_limit=page_limit)


@app.task(bind=True, default_retry_delay=10)
def cpk_library(self, library_id, max_retries=0, logger=None):
    cpk = CPK(logger)
    return cpk.library_parsed(library_id)


@app.task(bind=True, default_retry_delay=10)
def cpk_documents_overview(self, document_ids, max_retries=0, logger=None):
    return cpk_docs_overview(document_ids, logger)


@app.task(bind=True, default_retry_delay=10)
def cpk_document(self, document_id, max_retries=0, logger=None):
    return cpk_doc(document_id, logger)


@app.task(bind=True, default_retry_delay=10)
def cpk_unit(self, unit_id, max_retries=0, logger=None):
    cpk = CPK(logger)
    return cpk.unit(unit_id)
