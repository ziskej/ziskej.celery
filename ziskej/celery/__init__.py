# -*- coding: utf-8 -*-

"""Flask wrapper pro CPK API, různá NTK API a Získej API pro CPK pro čtenáře.
Celery workery a tasky pro asynchronní volání.  Samotný Fkask wrapper obsahuje
synchronní (blokující) i asynchronní (neblokující) části.

Používané prefixy pro konstrukci URI:

    /cpkapi/
    Prefix pro wrapper pro CPK API.

    /ntkapi/
    Prefix pro wrapper pro různá API v rámci NTK.

    /api/
    Prefix pro Získej API pro CPK pro čtenáře.
"""

from .app import app
